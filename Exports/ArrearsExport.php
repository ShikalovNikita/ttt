<?php
namespace App\SubSystems\OneC\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class ArrearsExport implements FromArray
{
    protected $arrears;

    public function __construct(array $arrears)
    {
        $this->arrears = $arrears;
    }

    public function array(): array
    {
        return $this->arrears;
    }
}
