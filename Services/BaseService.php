<?php


namespace App\SubSystems\OneC\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;

abstract class BaseService
{
    protected $baseUrl;
    protected $httpClient;
    protected $entityService = '';

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = "{$baseUrl}/".$this->entityService;
        $this->httpClient = new Client([
            RequestOptions::AUTH => [
                config('1C.LOGIN'),
                config('1C.PASSWORD')
            ]
        ]);
    }

    /**
     * @param array $query
     * @return mixed
     */
    public function get(array $query)
    {
        return json_decode(
            $this->httpClient->get($this->baseUrl.'/get', [
                RequestOptions::QUERY => $query
            ])->getBody()->getContents(),
            true
        );
    }

    public function main(array $attributes)
    {
        return json_decode(
            $this->httpClient->post($this->baseUrl, [
                RequestOptions::JSON => $attributes
            ])->getBody()->getContents(),
            true
        );
    }

    public function getBy(array $attributes)
    {
        return json_decode(
            $this->httpClient->post($this->baseUrl.'/get', [
                RequestOptions::JSON => $attributes
            ])->getBody()->getContents()
        );
    }

    public function search(array $attributes)
    {
        return json_decode(
            $this->httpClient->post($this->baseUrl.'/search', [
                RequestOptions::JSON => $attributes
            ])->getBody()->getContents()
        );
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        try
        {
            $response = $this->httpClient->post($this->baseUrl.'/create', [
                RequestOptions::JSON => $attributes
            ])->getBody()->getContents();

            return json_decode($response, true);
        }
        catch (RequestException $exception)
        {
            return json_decode($exception->getResponse()->getBody()->getContents(), true);
        }
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function update(array $attributes)
    {
        try
        {
            $response = $this->httpClient->post($this->baseUrl.'/update', [
                RequestOptions::JSON => $attributes
            ])->getBody()->getContents();

            return json_decode($response, true);
        }
        catch (RequestException $exception)
        {
            return json_decode($exception->getResponse()->getBody()->getContents(), true);
        }
    }
}
