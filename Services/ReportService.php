<?php


namespace App\SubSystems\OneC\Services;


use GuzzleHttp\RequestOptions;

class ReportService extends BaseService
{
    protected $entityService = 'report';

    public function arrearsByPeriod($startDate, $endDate)
    {
        return json_decode($this->httpClient->post($this->baseUrl.'/amo-arrears', [
            RequestOptions::JSON => [
                'start' => $startDate,
                'end' => $endDate
            ]
        ])->getBody()->getContents(), true);
    }
}
