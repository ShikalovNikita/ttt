<?php


namespace App\SubSystems\OneC\Services;


class GoodsService extends BaseService
{
    protected $entityService = 'goods';

    public function suggest($filter, $currency_uid = '3e28039f-4b55-11e7-99c0-0030489f3c9f')
    {
        return json_decode(
            $this->httpClient->get($this->baseUrl."/suggest?filter={$filter}&currency_uid={$currency_uid}")->getBody()->getContents(),
            true
        );
    }
}
