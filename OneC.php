<?php


namespace App\SubSystems\OneC;

use App\SubSystems\OneC\Services\AgreementService;
use App\SubSystems\OneC\Services\ClientService;
use App\SubSystems\OneC\Services\ContractService;
use App\SubSystems\OneC\Services\GoodsService;
use App\SubSystems\OneC\Services\InvoiceService;
use App\SubSystems\OneC\Services\OrganizationService;
use App\SubSystems\OneC\Services\ProductService;
use App\SubSystems\OneC\Services\ReportService;
use App\SubSystems\OneC\Services\StockService;
use App\SubSystems\OneC\Services\SubdivisionService;
use App\SubSystems\OneC\Services\TaskService;
use App\SubSystems\OneC\Services\UserService;

class OneC
{
    /**
     * Базовый URL
     * @var string
     */
    private $baseUrl;

    /**
     * OneC constructor.
     * @param $host
     * @param $database
     * @param $root
     */
    public function __construct($host, $database, $root)
    {
        $this->baseUrl = "{$host}/{$database}/hs/{$root}";
    }

    /**
     * @return InvoiceService
     */
    public function invoices() : InvoiceService
    {
        return new InvoiceService($this->baseUrl);
    }

    /**
     * @return ReportService
     */
    public function report(): ReportService
    {
        return new ReportService($this->baseUrl);
    }

    /**
     * @return OrganizationService
     */
    public function organizations() : OrganizationService
    {
        return new OrganizationService($this->baseUrl);
    }

    /**
     * @return ContractService
     */
    public function contracts(): ContractService
    {
        return new ContractService($this->baseUrl);
    }

    /**
     * @return ClientService
     */
    public function clients() : ClientService
    {
        return new ClientService($this->baseUrl);
    }

    /**
     * @return StockService
     */
    public function stocks() : StockService
    {
        return new StockService($this->baseUrl);
    }

    /**
     * @return SubdivisionService
     */
    public function subdivisions() : SubdivisionService
    {
        return new SubdivisionService($this->baseUrl);
    }

    /**
     * @return UserService
     */
    public function users() : UserService
    {
        return new UserService($this->baseUrl);
    }

    /**
     * @deprecated
     * @return GoodsService
     */
    public function goodsSuggests() : GoodsService
    {
        return new GoodsService($this->baseUrl);
    }

    public function productPrice($uid)
    {
        $service = new ProductService($this->baseUrl);
        return $service->price($uid);
    }

    /**
     * @return ProductService
     */
    public function products(): ProductService
    {
        return new ProductService($this->baseUrl);
    }

    /**
     * @return AgreementService
     */
    public function agreements() : AgreementService
    {
        return new AgreementService($this->baseUrl);
    }

    /**
     * @return TaskService
     */
    public function tasks() : TaskService
    {
        return new TaskService($this->baseUrl);
    }
}
