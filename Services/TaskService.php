<?php


namespace App\SubSystems\OneC\Services;


use GuzzleHttp\RequestOptions;

class TaskService extends BaseService
{
    protected $entityService = 'task';

    public function addMessage(array $attributes)
    {
        return json_decode($this->httpClient->post($this->baseUrl.'/add-message', [
            RequestOptions::JSON => $attributes
        ])->getBody()->getContents(), true);
    }
}
