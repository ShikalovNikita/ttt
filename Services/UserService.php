<?php


namespace App\SubSystems\OneC\Services;


use GuzzleHttp\RequestOptions;

class UserService extends BaseService
{
    protected $entityService = 'user';

    public function settings(array $attributes) {
        return json_decode($this->httpClient->post($this->baseUrl.'/settings', [
            RequestOptions::JSON => $attributes
        ])->getBody()->getContents(), true);
    }

}
