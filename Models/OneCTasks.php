<?php

namespace App\SubSystems\OneC\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * OneCTasks
 *
 * @mixin Eloquent
 */
class OneCTasks extends Model {
    protected $table = "onec_tasks";

    protected $fillable = [
        'entity_id',
        'user_from',
        'user_to',
        'user_uid',
        'task_number',
        'task_uid'
    ];

}
