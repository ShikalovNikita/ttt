<?php


namespace App\SubSystems\OneC\Services;

use GuzzleHttp\RequestOptions;

class ProductService extends BaseService
{
    protected $entityService = 'products';

    public function price($uid) {
        return json_decode($this->httpClient->get($this->baseUrl."/price?product_uid={$uid}")->getBody()->getContents(),true);
    }

    public function reserve(array $attributes) {
	return json_decode($this->httpClient->post($this->baseUrl."/reserve", [
		RequestOptions::JSON => $attributes
	])->getBody()->getContents(), true);
    }
}
