<?php


namespace App\SubSystems\OneC\Services;


use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

class InvoiceService extends BaseService
{
    protected $entityService = 'invoice';

    public function print($uid, $discount = 'false', $ext = 'pdf')
    {
        return $this->httpClient->get($this->baseUrl."/{$uid}/print?discount={$discount}&ext={$ext}")->getBody()->getContents();
    }

    public function link(array $attributes)
    {
        return json_decode($this->httpClient->post($this->baseUrl.'/link', [
                    RequestOptions::JSON => $attributes
                ])->getBody()->getContents(), true);
    }

    public function reserve(array $attributes)
    {
        return json_decode($this->httpClient->post($this->baseUrl.'/reserve', [
                    RequestOptions::JSON => $attributes
                ])->getBody()->getContents(), true);
    }

    public function createSimple(array $attributes)
    {
        try
        {
            $response = $this->httpClient->post($this->baseUrl.'/create_simple', [
                RequestOptions::JSON => $attributes
            ])->getBody()->getContents();

            return json_decode($response, true);
        }
        catch (RequestException $exception)
        {
            return json_decode($exception->getResponse()->getBody()->getContents(), true);
        }
    }
}
